import React, {Component} from 'react';
import './Root.scss';
import {Container} from "react-bootstrap";
import {Provider} from 'mobx-react';
import RootStore from "./stores/RootStore";
import AppContainer from "./containers/AppContainer";

class Root extends Component {
  render() {
    return (
      <Provider store={RootStore}>
        <div className={'position-fixed background-taxi'}/>
        <Container>
          <AppContainer/>
        </Container>
      </Provider>
    );
  }
}

export default Root;
