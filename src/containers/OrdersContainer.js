import React from "react";
import BaseContainer from "./BaseContainer";
import OrdersListComponent from "../components/OrdersListComponent";
import {inject, observer} from "mobx-react";
import {values} from "mobx";

@inject('store') @observer
class OrdersContainer extends BaseContainer {
  sort = (a, b) => {
    if (a.id < b.id) return 1;
    else if (a.id > b.id) return -1;
    else return 0;
  };

  render() {
    return (
      <>
        <OrdersListComponent data={values(this.commonStore.orders).sort(this.sort)} title={'Заказы'}/>
      </>
    )
  }
}

export default OrdersContainer;
