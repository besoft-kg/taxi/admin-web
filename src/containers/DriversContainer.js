import React from "react";
import BaseContainer from "./BaseContainer";
import {Card, Button, Table} from "react-bootstrap";
import requester from "../utils/requester";
import {inject, observer} from "mobx-react";
import {LinkContainer} from 'react-router-bootstrap';

@inject('store') @observer
class DriversContainer extends BaseContainer {
  componentDidMount() {
    requester.get('/admin/driver').then((response) => {
      if (response.status === 'success') {
        this.commonStore.createOrUpdate('driver', [
          ...response.payload.items,
          ...response.payload.not_confirmed_items
        ]);
      }
    }).catch((e) => {
      console.log(e);
    });
  }

  sort = (a, b) => {
    if (a.id < b.id) return 1;
    else if (a.id > b.id) return -1;
    else return 0;
  };

  render() {
    const not_confirmed_drivers = this.commonStore.not_confirmed_drivers;
    const confirmed_drivers = this.commonStore.confirmed_drivers;

    return (
      <>
        {not_confirmed_drivers.length > 0 && (
          <Card className={'mb-3 border-radius-top-0'}>
            <Card.Body>
              <Card.Title>Неподтвержденные водители</Card.Title>

              <Table striped hover>
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Тел.номер</th>
                  <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                {not_confirmed_drivers.sort(this.sort).map(v => (
                  <tr key={v.id}>
                    <td>{v.id}</td>
                    <td>{`+${v.phone_number}`}</td>
                    <td>
                        <LinkContainer to={`/drivers/view/${v.id}`}>
                          <Button size={'sm'}>Подробнее</Button>
                        </LinkContainer>
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </Card.Body>
          </Card>
        )}

        <Card className={not_confirmed_drivers.length === 0 ?'border-radius-top-0' : ''}>
          <Card.Body>
            <Card.Title>Водители</Card.Title>

            <Table striped hover>
              <thead>
              <tr>
                <th>ID</th>
                <th>Тел.номер</th>
                <th>Полное имя</th>
                <th>Пол</th>
                <th>Категории</th>
                <th>Гос.номер</th>
                <th>Действия</th>
              </tr>
              </thead>
              <tbody>
              {confirmed_drivers.sort(this.sort).map(v => (
                <tr key={v.id}>
                  <td>{v.id}</td>
                  <td>{`+${v.phone_number}`}</td>
                  <td>{v.full_name}</td>
                  <td>{v.ui_gender}</td>
                  <td>{v.ui_categories}</td>
                  <td>{v.state_reg_plate}</td>
                  <td>
                    <LinkContainer to={`/drivers/view/${v.id}`}>
                      <Button size={'sm'}>Подробнее</Button>
                    </LinkContainer>
                  </td>
                </tr>
              ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </>
    );
  }
}

export default DriversContainer;
