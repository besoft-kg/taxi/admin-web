import {Button, Card, Col, Form, Row} from "react-bootstrap";
import React from "react";
import BaseContainer from "./BaseContainer";
import {inject, observer} from "mobx-react";
import {action, computed, observable} from "mobx";
import PhoneNumber from "../utils/PhoneNumber";
import requester from "../utils/requester";

@inject('store') @observer
class AuthContainer extends BaseContainer {
  @observable phone_number = '';
  @observable loading = false;
  @observable response_status = '';
  @observable verification_code = '';

  constructor(props) {
    super(props);

    this.phone_object = new PhoneNumber();
  }


  @action setPhoneNumber = (v) => {
    this.phone_number = v;
    this.phone_object.parseFromString(v);
  };

  @computed get phone_is_valid() {
    return this.phone_number.length > 0 && this.phone_object && this.phone_object.isValid() && !this.loading;
  }

  @action sendCode = async () => {
    if (!this.phone_is_valid || this.loading) return;

    try {
      this.loading = true;
      const response = await requester.post('/admin/auth/phone', {
        phone_number: this.phone_object.getE164Format().slice(1),
      });

      switch (response.status) {
        case 'code_is_sent':
          alert('Код отправлено!');
          break;

        case 'code_was_sent':
          alert('Код был отправлен!');
          break;

        case 'admin_not_found':
          alert('Админ не найден!');
          break;

        default:
          break;
      }

      if (['code_was_sent', 'code_is_sent'].includes(response.status)) {
        this.response_status = response.status;
      }
    } catch (e) {}
    finally {
      this.loading = false;
    }
  };

  @action checkCode = async () => {
    if (!this.phone_is_valid || this.loading) return;

    try {
      this.loading = true;
      const response = await requester.post('/admin/auth/phone/login', {
        phone_number: this.phone_object.getE164Format().slice(1),
        version_code: 1,
        platform: 'web',
        code: this.verification_code,
      });

      switch (response.status) {
        case 'code_is_incorrect':
          alert('Неправильный код!');
          break;

        case 'code_is_invalid':
          alert('Код недействительный!');
          break;

        case 'admin_not_found':
          alert('Админ не найден!');
          break;

        case 'success':
          alert('Добро пожаловать!');
          this.appStore.makeAuth(response.payload.user, response.payload.token);
          break;

        default:
          break;
      }
    } catch (e) {}
    finally {
      this.loading = false;
    }
  };

  render() {
    return (
      <Row className={'justify-content-center'}>
        <Col lg={4} xl={4} sm={11} xs={11} md={6}>
          <Card
            style={{
              marginTop: '50vh',
              transform: 'translateY(-50%)',
              backgroundColor: 'rgba(255, 255, 255, 0.7)',
            }}
          >
            <Card.Body className={'text-center'}>
              <h1 className={'h3 mb-3'}>Добро пожаловать!</h1>
              <div>
                <Form.Control disabled={this.loading} value={this.phone_number} onChange={(v) => this.setPhoneNumber(v.target.value)} type="number" placeholder="Телефон номер" />
                {this.response_status !== '' && (
                  <Form.Control className={'mt-2'} disabled={this.loading} value={this.verification_code} onChange={(v) => this.setValue('verification_code', v.target.value)} type="number" placeholder="Код подтверждения" />
                )}
                <Button block className={'mt-2'} disabled={!this.phone_is_valid || (this.response_status !== '' && this.verification_code.length !== 4)} onClick={() => this.response_status === '' ? this.sendCode() : this.checkCode()}>{this.response_status === '' ? 'Получить код' : 'Войти в систему'}</Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default AuthContainer;
