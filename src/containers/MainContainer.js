import React from 'react';
import {inject, observer} from 'mobx-react';
import BaseContainer from "./BaseContainer";
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import HomeContainer from "./HomeContainer";
import {Nav, Navbar, Spinner} from "react-bootstrap";
import requester from "../utils/requester";
import DriversContainer from "./DriversContainer";
import Pusher from 'pusher-js';
import {API_URL, APP_NAME} from "../utils/settings";
import {values} from "mobx";
import DriversViewContainer from "./DriversViewContainer";
import OrdersContainer from "./OrdersContainer";

@inject('store') @observer
class MainContainer extends BaseContainer {
  componentDidMount() {
    requester.get('/admin/category').then((response) => {
      this.commonStore.createOrUpdate('category', response.payload);

      requester.get('/admin/order').then((response) => {
        this.commonStore.createOrUpdate('order', response.payload);
      }).catch((e) => {
        console.log(e);
      });
    }).catch((e) => {
      console.log(e);
    });

    Pusher.logToConsole = true;

    const pusher = new Pusher('8c0e90a6896b7c598920', {
      cluster: 'eu',
      authEndpoint: `${API_URL}/broadcasting/auth`,
      auth: {
        headers: {
          'Authorization': `Bearer ${this.appStore.token}`,
        },
      },
    });

    const order_channel = pusher.subscribe('private-admin.orders');
    const driver_channel = pusher.subscribe('private-admin.drivers');

    order_channel.bind('on-create', (data) => {
      this.commonStore.createOrUpdate('order', data.item);
    });

    order_channel.bind('on-update', (data) => {
      this.commonStore.createOrUpdate('order', data.item);
    });

    driver_channel.bind('on-update', (data) => {
      this.commonStore.createOrUpdate('driver', data.item);
    });
  }

  render() {
    if (values(this.commonStore.categories).length === 0) {
      return (
        <div>
          <Spinner />
        </div>
      );
    }

    return (
      <Router>
        <div className={'py-5'}>
          <Navbar style={{borderTopLeftRadius: 10, borderTopRightRadius: 10}} bg="primary" variant="dark">
            <Navbar.Brand href="#" as={Link} to={'/'}>{APP_NAME}</Navbar.Brand>
            <Nav className="ml-auto">
              <Nav.Link to={'/'} as={Link} href="#">Главная</Nav.Link>
              <Nav.Link to={'/drivers'}  as={Link} href="#">Водители</Nav.Link>
              <Nav.Link href="#pricing">Админы</Nav.Link>
              <Nav.Link href="#pricing">Клиенты</Nav.Link>
              <Nav.Link to={'/orders'} as={Link} href="#">Заказы</Nav.Link>
            </Nav>
          </Navbar>

          <Switch>
            <Route exact path="/">
              <HomeContainer/>
            </Route>
            <Route exact path="/drivers">
              <DriversContainer/>
            </Route>
            <Route exact path="/drivers/view/:id">
              <DriversViewContainer/>
            </Route>
            <Route exact path="/orders">
              <OrdersContainer/>
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default MainContainer;
