import React from "react";
import BaseContainer from "./BaseContainer";
import {Card, Spinner, Image, FormControl, FormCheck, Button} from "react-bootstrap";
import requester from "../utils/requester";
import {inject, observer} from "mobx-react";
import {action, computed, observable, values} from "mobx";
import {withRouter} from 'react-router';

@withRouter @inject('store') @observer
class DriversViewContainer extends BaseContainer {
  @observable gender = null;
  @observable full_name = '';
  @observable categories = [];
  @observable state_reg_plate = '';
  @observable drivers_license_expiration_date = null;

  componentDidMount() {
    requester.get('/admin/driver', {
      id: this.props.match.params.id,
    }).then((response) => {
      if (response.status === 'success') {
        this.commonStore.createOrUpdate('driver', response.payload);
      }
    }).catch((e) => {
      console.log(e);
    });
  }

  confirm = () => {
    if (!this.item) return;

    requester.post('/admin/driver/confirm',{
      id: this.item.id,
      gender: this.gender,
      full_name: this.full_name,
      categories: this.categories,
      state_reg_plate: this.state_reg_plate,
      drivers_license_expiration_date: this.drivers_license_expiration_date,
    }).then(response => {
      console.log(response);
      if (response.status === 'success') {
        this.commonStore.createOrUpdate('driver', response.payload);
      }
    }).catch(e => {
      console.log(e);
    })
  }

  sort = (a, b) => {
    if (a.id < b.id) return 1;
    else if (a.id > b.id) return -1;
    else return 0;
  };

  @computed get item() {
    return this.commonStore.drivers.get(this.props.match.params.id);
  }

  genders = [
    {title: 'женский', value: 'female'},
    {title: 'мужской', value: 'male'},
  ];

  @action setCategories = (id, checked) => {
    if (checked) {
      if (!this.categories.includes(id)) this.categories.push(id);
    } else {
      const index = this.categories.indexOf(id);
      if (index !== -1) this.categories.splice(index, 1);
    }
  };

  @computed get can_be_confirmed() {
    if (!this.item) return false;
    return this.item.can_be_confirmed && this.categories.length > 0 && this.drivers_license_expiration_date && this.full_name && this.gender && this.state_reg_plate;
  }

  render() {
    if (!this.item) {
      return (
        <div>
          <Spinner/>
        </div>
      );
    }

    return (
      <Card style={{backgroundColor: '#eee'}}>
        <Card.Body>
          <div className={'d-flex justify-content-start'}>
            {this.item.picture ? (
              <Image style={{}} width={200} src={this.item.picture.url.original}/>
            ) : (
              <Card style={{width:200, height:200}}>
              <div className={'text-center'}>No picture</div>
              </Card>
            )}
            <div className={'ml-3'} style={{flexGrow: 1}}>
              {!this.item.is_confirmed ? (
                <>
                  <FormControl type="text" placeholder="Полное имя" onChange={({target}) => this.setValue('full_name', target.value)} size={'sm'} />
                  <FormControl className={'my-2'} type="text" placeholder="Гос.номер (06KG226AEB)" onChange={({target}) => this.setValue('state_reg_plate', target.value)} size={'sm'} />
                  <FormControl type="date" placeholder="Drivers license" onChange={({target}) => this.setValue('drivers_license_expiration_date', target.value)} size={'sm'} />
                  <div className={'my-2'}>
                    {this.genders.map(({title, value}) => (
                      <FormCheck
                        onChange={() => this.setValue('gender', value)}
                        name={'gender'}
                        className={`d-inline${value === 'male' ? ' ml-5' : ''}`}
                        type={'radio'}
                        id={`gender-${value}`}
                        label={title}
                      />
                    ))}
                  </div>
                  <div>
                    {values(this.commonStore.categories).map((v, i) => (
                      <FormCheck
                        onChange={({target}) => this.setCategories(v.id, target.checked)}
                        className={`d-inline${i > 0 ? ' ml-5' : ''}`}
                        type={'checkbox'}
                        id={`category-${v.id}`}
                        label={v.title}
                      />
                    ))}
                  </div>
                  <div className={'d-flex justify-content-end mt-2'}>
                    <Button size={'sm'} disabled={!this.can_be_confirmed} onClick={this.confirm}>
                       Подвердить
                    </Button>
                  </div>
                </>
              ) : (
                <>
                  <h1 className={'h3'}>{this.item.full_name}</h1>
                  <div className={'text-muted'}>Гос.номер: <strong>{this.item.state_reg_plate}</strong></div>
                  <div className={'text-muted'}>Тел.номер: <strong>{this.item.phone_number}</strong></div>
                  <div className={'text-muted'}>Категории: <strong>{this.item.ui_categories}</strong></div>
                  <div className={'text-muted'}>Пол: <strong>{this.item.ui_gender}</strong></div>
                </>
              )}
            </div>
          </div>

          <div className={'d-flex mt-3 justify-content-around'}>
            {['datasheet_picture', 'vehicle_picture', 'drivers_license_picture'].map(v => (
              <>
                {this.item[v] ? (
                  <div style={{flexGrow: 1, textAlign: 'center'}}>
                    <Image width={300} src={this.item[v].url.original}/>
                  </div>
                ) : (
                  <Card style={{width: 300, height: 300}}>
                    <div className={'text-center'} style={{flexGrow: 1}}>
                      No photo
                    </div>
                  </Card>
                )}
              </>
            ))}
          </div>
        </Card.Body>
      </Card>
    );
  }
}

export default DriversViewContainer;
