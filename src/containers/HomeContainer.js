import React from "react";
import BaseContainer from "./BaseContainer";
import CreateOrderFormComponent from "../components/CreateOrderFormComponent";
import {values} from "mobx";
import OrdersListComponent from "../components/OrdersListComponent";
import {inject, observer} from "mobx-react";

@inject('store') @observer
class HomeContainer extends BaseContainer {
  sort = (a, b) => {
    if (a.id < b.id) return 1;
    else if (a.id > b.id) return -1;
    else return 0;
  };

  render() {
    return (
      <>
        <CreateOrderFormComponent/>
        <div className={'my-5'}>
          <OrdersListComponent data={values(this.commonStore.orders).sort(this.sort).slice(0, 5)} title={'Последние заказы'} />
        </div>
      </>
    );
  }
}

export default HomeContainer;
