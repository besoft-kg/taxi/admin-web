import React from 'react';
import {inject, observer} from 'mobx-react';
import BaseContainer from "./BaseContainer";
import AuthContainer from "./AuthContainer";
import storage from "../utils/storage";
import MainContainer from "./MainContainer";

@inject('store') @observer
class AppContainer extends BaseContainer {
  componentDidMount() {
    this.appStore.checkAuth(
      storage.get('user'),
      storage.get('token')
    );
  }

  render() {
    if (!this.appStore.authenticated) {
      return <AuthContainer/>;
    }

    return <MainContainer/>;
  }
}

export default AppContainer;
