import React, {Component} from 'react';
import {action} from "mobx";

class BaseContainer extends Component {
  constructor(props) {
    super(props);

    if ('store' in this.props) {
      this.store = this.props.store;
      this.appStore = this.store.appStore;
      this.commonStore = this.store.commonStore;
    }
  }

  @action setValue = (name, value) => this[name] = value;
}

export default BaseContainer;
