import {types as t} from "mobx-state-tree";

export default t
  .model('bi',{
    id: t.identifierNumber,
    name: t.string,
    value: t.string,
    created_at: t.Date,
    updated_at: t.Date,
  });
