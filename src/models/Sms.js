import {types as t} from "mobx-state-tree";

export default t
  .model('sms', {
    id: t.identifierNumber,
    external_id: t.maybeNull(t.integer),
    phone_number: t.number,
    country: t.maybeNull(t.string),
      region: t.maybeNull(t.string),
      operator: t.maybeNull(t.string),
      content: t.string,
      parts: t.integer,
      amount: t.integer,
      price: t.integer,
      status: t.string,
      response: t.maybeNull(t.string),
      delivery_report: t.maybeNull(t.string),
      used_for: t.maybeNull(t.string),
      payload: t.maybeNull(t.frozen()),
      service: t.string,
      test: t.integer,
    created_at: t.Date,
    updated_at: t.Date,
  });
