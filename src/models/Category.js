import {types as t} from "mobx-state-tree";

export default t
  .model('category',{
    id: t.identifierNumber,
    title: t.string,
    description: t.maybeNull(t.string),
    icon: t.maybeNull(t.string),
    landing_price: t.maybeNull(t.integer),
    price_for_km: t.maybeNull(t.integer),
    price_for_waiting_minute: t.maybeNull(t.integer),
    price_for_duration_minute: t.maybeNull(t.integer),
    parent_id: t.maybeNull(t.integer),
    created_at: t.Date,
    //updated_at: t.Date,
  });
