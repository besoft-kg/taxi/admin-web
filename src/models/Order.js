import {types as t} from "mobx-state-tree";
import Category from "./Category";
import Admin from "./Admin";
import Client from "./Client";
import float from "./float";

export default t
  .model('order', {
    id: t.identifierNumber,
    publisher_id: t.maybeNull(t.integer),
    publisher: t.maybeNull(t.reference(Admin)),
    client_id: t.maybeNull(t.integer),
    client: t.maybeNull(t.reference(Client)),
    category_id: t.integer,
    category: t.maybeNull(t.reference(Category)),
    phone_number: t.maybeNull(t.string),
    status: t.enumeration([
      'pending',
      'fake_call',
      'cancelled_by_client',
      'cancelled_by_driver',
      'cancelled_by_admin',
      'going_to_client',
      'in_place',
      'going_with_client',
      'completed',
    ]),
    note: t.maybeNull(t.string),
    history: t.maybeNull(t.array(t.frozen())),
    payload: t.maybeNull(t.frozen()),
    address: t.maybeNull(t.string),
    address_lat: t.maybeNull(float),
    address_lng: t.maybeNull(float),
    created_at: t.Date,
    updated_at: t.Date,
  }).views(self => ({

    get ui_status() {
      switch(self.status) {
        case 'pending':
          return 'в ожидании';
        case 'going_to_client':
          return 'принят';
        case 'cancelled_by_admin':
          return 'отменено';
        default:
          return 'неизвестный';
      }
    },

    get cancelled() {
      return self.status.startsWith('cancelled_by_');
    },

    get address_text() {
      if (self.address && self.address_lat && self.address_lng) return `${self.address}, ${self.address_lat}@${self.address_lng}`;
      if (self.address) return self.address;
      else return `${self.address_lat}@${self.address_lng}`;
    },
  }));
