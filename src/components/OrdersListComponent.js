import {Button, Card, Table} from "react-bootstrap";
import React from "react";
import BaseComponent from "./BaseComponent";
import {inject, observer} from "mobx-react";
import requester from "../utils/requester";

@inject('store') @observer
class OrdersListComponent extends BaseComponent {
  cancel = (item) => {
    if (!item.status.cancelled && !window.confirm('Вы уверены?')) return;

    requester.post('/admin/order/cancel', {
      id: item.id,
    }).then(response => {
      this.commonStore.createOrUpdate('order', response.payload);
    }).catch(e => {
      console.log(e);
    });
  };

  active = (item) => {
    if (item.status.cancelled && !window.confirm('Вы уверены?')) return;

    requester.post('/admin/order/active', {
      id: item.id,
    }).then(response => {
      this.commonStore.createOrUpdate('order', response.payload);
    }).catch(e => {
      console.log(e);
    });
  };

  render() {
    return (
      <Card>
        <Card.Body>
          <Card.Title>{this.props.title}</Card.Title>

          <Table striped hover>
            <thead>
              <tr>
                <th>ID</th>
                <th>Номер клиента</th>
                <th>Адрес</th>
                <th>Категория</th>
                <th>Статус</th>
                <th>Действия</th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map(v => (
                <tr key={v.id}>
                  <td>{v.id}</td>
                  <td>{`+${v.phone_number}`}</td>
                  <td>{v.address_text}</td>
                  <td>{v.category.title}</td>
                  <td>{v.ui_status}</td>
                  <td>
                    {v.cancelled ? (
                      <Button block onClick={() => this.active(v)} variant={'info'} size={'sm'}>Активировать</Button>
                    ) : (
                      <Button block onClick={() => this.cancel(v)} variant={'danger'} size={'sm'}>Отменить</Button>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card.Body>
      </Card>
    );
  }
}

export default OrdersListComponent;
