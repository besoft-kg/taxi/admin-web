import {Card, Col, Row, Form, Button, FormControl} from "react-bootstrap";
import {GoogleMap, LoadScript, Marker, StandaloneSearchBox} from "@react-google-maps/api";
import {GOOGLE_CLOUD_API_KEY} from "../utils/settings";
import React from "react";
import BaseComponent from "./BaseComponent";
import {inject, observer} from "mobx-react";
import {action, computed, observable, values} from "mobx";
import PhoneNumber from "../utils/PhoneNumber";
import requester from "../utils/requester";

@inject('store') @observer
class CreateOrderFormComponent extends BaseComponent {
  searchbox_ref = React.createRef();
  libraries = ['places'];

  @observable address = '';
  @observable address_lat = '40.533984';
  @observable address_lng = '72.794552';
  @observable phone_number = '';
  @observable note = '';
  @observable loading = false;
  @observable category_id = 1;

  constructor(props) {
    super(props);

    this.phone_object = new PhoneNumber();
  }

  @computed get phone_is_valid() {
    return this.phone_number.length > 0 && this.phone_object && this.phone_object.isValid();
  }

  @action clear = (force = false) => {
    if (!force && !window.confirm('Вы уверены?')) return;

    this.address = '';
    this.address_lat = '40.533984';
    this.address_lng = '72.794552';
    this.phone_number = '';
    this.phone_object = new PhoneNumber();
    this.category_id = 1;
    this.note = '';
  };

  @computed get clearable() {
    return (
      this.address.length > 0 ||
      `${this.address_lat}`.length > 0 ||
      `${this.address_lng}`.length > 0 ||
      this.phone_number.length > 0 ||
      this.note.length > 0
    );
  };

  @computed get can_be_sent() {
    return (
      ((`${this.address_lng}`.length > 0 && `${this.address_lat}`.length > 0) || this.address.length > 0) &&
      this.phone_is_valid &&
      this.category_id > 0
    );
  }

  onChangeSearchBox = (data) => {
    data = data[0];
    const {geometry} = data;
    this.setValue('address', data.name);
    this.setValue('address_lat', geometry.location.lat());
    this.setValue('address_lng', geometry.location.lng());
    console.log(data);
  };

  @action setPhoneNumber = (v) => {
    this.phone_number = v;
    this.phone_object.parseFromString(v);
  };

  @action send = async (clear) => {
    if (this.loading) return;

    this.loading = true;

    try {
      const response = await requester.post('/admin/order', {
        category_id: this.category_id,
        phone_number: this.phone_object.getE164Format().slice(1),
        address: this.address,
        address_lng: this.address_lng,
        address_lat: this.address_lat,
        note: this.note,
      });

      if (response.status === 'success') {
        //this.commonStore.createOrUpdate('order', response.payload);
      }

      if (clear) this.clear(true);
    } catch (e) {

    } finally {
      this.loading = false;
    }
  };

  render() {
    return (
      <Card style={{borderRadius: 0}}>
        <Card.Body>
          <Row>
            <Col md={7}>
              <LoadScript
                language={'ru-RU'}
                libraries={this.libraries}
                googleMapsApiKey={GOOGLE_CLOUD_API_KEY}
              >
                <GoogleMap
                  mapContainerStyle={{
                    width: '100%',
                    height: '400px',
                  }}
                  center={{lat: +this.address_lat, lng: +this.address_lng,}}
                  zoom={15}
                  onClick={(e) => {
                    this.setValue('address_lat', e.latLng.lat());
                    this.setValue('address_lng', e.latLng.lng());
                  }}
                >
                  { /* Child components, such as markers, info windows, etc. */}
                  <>
                    <Marker
                      draggable={true}
                      onDragEnd={(e) => {
                        console.log(e);
                        this.setValue('address_lat', e.latLng.lat());
                        this.setValue('address_lng', e.latLng.lng());
                      }}
                      position={{lat: +this.address_lat, lng: +this.address_lng,}}
                      onClick={() => {
                      }}
                    />
                  </>

                  <StandaloneSearchBox
                    onLoad={ref => this.searchbox_ref = ref}
                    onPlacesChanged={() => this.onChangeSearchBox(this.searchbox_ref.getPlaces())}
                  >
                    <FormControl
                      type={'text'}
                      placeholder={'Адрес'}
                      style={{
                        boxSizing: `border-box`,
                        border: `1px solid transparent`,
                        width: `270px`,
                        height: `42px`,
                        padding: `0 12px`,
                        borderRadius: `3px`,
                        boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                        fontSize: `14px`,
                        outline: `none`,
                        textOverflow: `ellipses`,
                        position: 'absolute',
                        top: '10px',
                        right: '10px',
                      }}
                    />
                  </StandaloneSearchBox>
                </GoogleMap>
              </LoadScript>
            </Col>
            <Col md={5}>
              <div className={'d-flex flex-column h-100 justify-content-between'}>
                <div>
                  <h3>Новый заказ</h3>
                  <div>
                    <Form.Control
                      onChange={({target}) => this.setValue('note', target.value)} value={this.note}
                      className={'mb-2'} size="sm" type="text" placeholder="Примечание"/>
                    <Form.Control
                      onChange={({target}) => this.setPhoneNumber(target.value)}
                      value={this.phone_number} className={'mb-2'} size="sm" type="number"
                      placeholder="Телефон номер"/>
                    <Form.Control
                      onChange={({target}) => this.setValue('address', target.value)} value={this.address}
                      className={'mb-2'} size="sm" type="text" placeholder="Адрес"/>
                    <Form.Control
                      value={`${this.address_lat}`.length > 0 && `${this.address_lng}`.length > 0 ? `${this.address_lat}, ${this.address_lng}` : ''}
                      readOnly className={'mt-2 mb-3'} size="sm" type="text" placeholder="Координаты адреса LAT, LNG"/>
                    <div>
                      {values(this.commonStore.categories).map(v => (
                        <div onClick={() => this.setValue('category_id', v.id)}
                             className={`selector-option border ${this.category_id === v.id ? 'border-primary text-primary bg-white' : null}`}>{v.title}</div>
                      ))}
                    </div>
                  </div>
                </div>
                <div className={'align-self-end'}>
                  <Button onClick={() => this.clear(false)} disabled={!this.clearable} size={'sm'} variant={'secondary'}
                          className={'mr-2'}>Сбросить</Button>
                  <Button variant={'info'} onClick={() => this.send(false)} className={'mr-2'}
                          disabled={!this.can_be_sent || this.loading} size={'sm'}>Отправить</Button>
                  <Button onClick={() => this.send(true)} disabled={!this.can_be_sent || this.loading} size={'sm'}>Отправить
                    и сбросить</Button>
                </div>
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    );
  }
}

export default CreateOrderFormComponent;
