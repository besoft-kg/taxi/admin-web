import {parsePhoneNumberFromString} from 'libphonenumber-js';

export default class PhoneNumber {
  constructor() {
    this.number = null;
  }

  parseFromString(s) {
    const n = parsePhoneNumberFromString(s, 'KG');
    if (n) this.number = n;
    return this;
  }

  getE164Format() {
    if (this.number === null) {
      return false;
    }
    return this.number.format('E.164');
  }

  getCountryCode() {
    if (this.number === null) {
      return false;
    }
    return this.number.country;
  }

  getNumber() {
    return this.number;
  }

  isValid() {
    if (this.number === null) {
      return false;
    }
    return this.number.isValid() && this.getCountryCode() === 'KG';
  }
}
