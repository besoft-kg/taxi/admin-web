import axios from 'axios';
import storage from "./storage";
import ServerError from "./ServerError";
import ConnectionError from "./ConnectionError";
import {API_URL} from "./settings";

const instance = axios.create({
  baseURL: `${API_URL}/api/v1`,
  timeout: 5000,
});

const request = async (method, url, data = {}) => {
  try {
    const headers = {};
    const token = storage.get('token');
    if (token) headers['Authorization'] = `Bearer ${token}`;

    const response = await instance.request({
      method,
      responseType: 'json',
      responseEncoding: 'utf8',
      url,
      data,
      headers,
    });

    if (response.data.result < 0) {
      if (response.data.status === "token_is_invalid") {
        storage.remove('token');
      }

      throw new ServerError(response);
    }

    return response;
  } catch (e) {
    if (e instanceof ServerError) {
      throw e;
    } else {
      if (e.response) {
        throw new ConnectionError(e.response);
      } else if (e.request) {
        throw new ConnectionError(e.response);
      } else {
        throw e;
      }
    }
  }
};

const post = async (url, data) => {
  try {
    const response = await request('post', url, data);
    return response.data;
  } catch (e) {
    alert(e.message);
    //throw e;
  }
};

const get = async (url, data) => {
  try {
    const response = await request('get', url, data);
    return response.data;
  } catch (e) {
    alert(e.message);
    //throw e;
  }
};

export default {
  post,
  get,
};
