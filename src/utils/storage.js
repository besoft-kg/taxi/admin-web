const get = (name, default_value = null) => {
  return JSON.parse(localStorage.getItem(`@Besoft_Taxi_${name}`)) || default_value;
};

const set = (name, value) => {
  return localStorage.setItem(`@Besoft_Taxi_${name}`, JSON.stringify(value));
};

const remove = (name) => {
  return localStorage.removeItem(`@Besoft_Taxi_${name}`);
};

export default {
  get,
  set,
  remove,
};
