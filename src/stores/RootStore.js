import {types as t} from 'mobx-state-tree';
import AppStore from './AppStore';
import CommonStore from "./CommonStore";

export default t
  .model('RootStore', {

    appStore: AppStore,
    commonStore: CommonStore,

  })
  .actions(self => ({}))
  .create({

    appStore: AppStore.create(),
    commonStore: CommonStore.create(),

  });
