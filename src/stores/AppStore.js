import {getRoot, getSnapshot, types as t} from 'mobx-state-tree';
import Admin from "../models/Admin";
import storage from "../utils/storage";
import requester from "../utils/requester";

export default t
  .model('AppStore', {

    user: t.maybeNull(t.reference(Admin)),
    token: t.maybeNull(t.string),
    auth_checking: t.optional(t.boolean, true),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const makeAuth = (user, token) => {
      storage.set('token', token);
      self.root.commonStore.createOrUpdate('admin', user);
      self.user = user.id;
      storage.set('user', self.user);
      self.token = token;
    };

    const clearUser = () => {
      self.user = null;
      self.token = null;
    };

    const checkAuth = (user, token) => {
      if (!token) return;

      if (user) {
        self.root.commonStore.createOrUpdate('admin', user);
        self.user = user.id;
      }
      self.token = token;

      requester.get('/admin/auth/me').then((response) => {
        self.root.commonStore.createOrUpdate('admin', response.payload);
        self.setValue('user', response.payload.id);
        storage.set('user', getSnapshot(self.user));
      }).catch(e => {
        self.clearUser();
      }).finally(() => {
        self.setValue('auth_checking', false);
      });
    };

    return {
      setValue,
      makeAuth,
      checkAuth,
      clearUser,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

    get authenticated() {
      return self.user !== null;
    },

  }));
