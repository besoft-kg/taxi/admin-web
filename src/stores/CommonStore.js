import {types as t} from 'mobx-state-tree';
import Admin from "../models/Admin";
import Category from "../models/Category";
import Order from "../models/Order";
import Driver from "../models/Driver";
import Image from "../models/Image";
import {values} from "mobx";

export default t
  .model('CommonStore', {

    admins: t.optional(t.map(Admin), {}),
    categories: t.optional(t.map(Category), {}),
    orders: t.optional(t.map(Order), {}),
    drivers: t.optional(t.map(Driver), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (type, item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(type, v));
        return;
      }

      switch (type) {
        case 'admin':
          self.admins.set(item.id, {
            ...item,
            last_action: new Date(item.last_action),
            created_at: new Date(item.created_at),
          });
          break;

        case 'category':
          self.categories.set(item.id, {
            ...item,
            created_at: new Date(item.created_at),
          });
          break;

        case 'order':
          self.orders.set(item.id, {
            ...item,
            category: self.categories.get(item.category_id),
            created_at: new Date(item.created_at),
            updated_at: new Date(item.updated_at),
          });
          break;

        case 'driver':
          self.drivers.set(item.id, {
            ...item,
            last_action: new Date(item.last_action),
            created_at: new Date(item.created_at),
            updated_at: new Date(item.updated_at),
            confirmed_at: item.confirmed_at ? new Date(item.confirmed_at) : null,
            drivers_license_expiration_date: item.drivers_license_expiration_date ? new Date(item.drivers_license_expiration_date) : null,

            picture: item.picture ? Image.create({
              ...item.picture,
              created_at: new Date(item.picture.created_at),
              updated_at: new Date(item.picture.updated_at),
            }) : null,

            datasheet_picture: item.datasheet_picture ? Image.create({
              ...item.datasheet_picture,
              created_at: new Date(item.datasheet_picture.created_at),
              updated_at: new Date(item.datasheet_picture.updated_at),
            }) : null,

            drivers_license_picture: item.drivers_license_picture ? Image.create({
              ...item.drivers_license_picture,
              created_at: new Date(item.drivers_license_picture.created_at),
              updated_at: new Date(item.drivers_license_picture.updated_at),
            }) : null,

            vehicle_picture: item.vehicle_picture ? Image.create({
              ...item.vehicle_picture,
              created_at: new Date(item.vehicle_picture.created_at),
              updated_at: new Date(item.vehicle_picture.updated_at),
            }) : null,
          });
          break;

        default:
          break;
      }
    };

    return {
      setValue,
      createOrUpdate,
    };

  })
  .views(self => ({

    get not_confirmed_drivers() {
      return values(self.drivers).filter(v => !v.is_confirmed);
    },

    get confirmed_drivers() {
      return values(self.drivers).filter(v => v.is_confirmed);
    }

  }));
